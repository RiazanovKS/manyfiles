package ru.rks;

import java.io.*;

/**
 * Класс считывает шестизначные числа из файла int6data.dat , отбирает из них счастливые и записывает их в файл txt6data.txt .
 * @author Рязанов К.С.
 */
public class ManyFiles3 {
    public static void main(String[] args) throws IOException {
        try (DataInputStream dataInputStream = new DataInputStream(new FileInputStream("int6data.dat"));
             BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("txt6data.txt"))) {
            int n;
            while (dataInputStream.available() != 0) {
                if (isHappy(n = dataInputStream.readInt())) {
                    bufferedWriter.write(String.valueOf(n) + "\n");
                }
            }
        }
    }

    /**
     * Метод складывает цифры числа n слева направо.
     * @param n
     * @return сумма цифр числа n слева направо.
     */
    public static int recursion(int n) {
        if (n < 10) {
            return n;
        } else {
            return n % 10 + recursion(n % 10);
        }
    }

    /**
     * Возвращает булевое значение в зависимости от того  ,счастливое число или нет.
     * @param n
     * @return true - если число счастливое , false - если нет.
     */
    public static boolean isHappy(int n) {
        if (recursion(n) == recursion(n / 1000)) {
            return true;
        } else {
            return false;
        }
    }


}



