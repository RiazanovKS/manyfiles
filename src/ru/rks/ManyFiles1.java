package ru.rks;

import java.io.*;


/**
 * Класс предназначен для считывания чисел из файла numbers.txt и записи в файл intdata.dat.
 *
 * @author Рязанов К.С.
 */
public class ManyFiles1 {
    public static void main(String[] args) throws IOException {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("numbers.txt"));
             DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(new File("intdata.dat")));
             BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("error.txt"))) {


            String string;


            while ((string = bufferedReader.readLine()) != null) {
               try {
                   dataOutputStream.writeInt(Integer.valueOf(string));
               }
               catch (NumberFormatException e){
                    bufferedWriter.write(string+ "\n");
               }
            }
        }
    }
}

