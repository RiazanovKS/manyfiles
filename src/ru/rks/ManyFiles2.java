package ru.rks;

import java.io.*;

import java.io.DataInputStream;

/**
 * Класс считывает целые числа из файла intdata.dat , отбирает шестизначные и записывает их в файл int6data.dat .
 *
 * @author Рязанов К.С.
 */
public class ManyFiles2 {
    public static void main(String[] args) throws IOException {
        try (DataInputStream dataInputStream = new DataInputStream(new FileInputStream("intdata.dat"));
             DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(new File("int6data.dat")))) {
            int n;
            while (dataInputStream.available() > 0) {
                if ((((n = dataInputStream.readInt()) > 999) && (n < 1000000))||(n==0)) {
                    dataOutputStream.writeInt(n);
                }
            }
        }
    }
}





